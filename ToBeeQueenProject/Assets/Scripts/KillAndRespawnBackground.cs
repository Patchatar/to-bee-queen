﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class KillAndRespawnBackground : MonoBehaviour
{

    public Sprite bg1;
    public Sprite bg2;
    public Sprite bg3;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if(collision.gameObject.tag == "Background")
        {
            Debug.Log("Background Entered Killzone");
            if (collision.gameObject.layer == 8)
            {
                Instantiate(bg1, new Vector3(20.23f, 0.02f, 0), Quaternion.identity);
            }
            if (collision.gameObject.layer == 9)
            {
                Instantiate(bg2, new Vector3(20.23f, 0.02f, 0), Quaternion.identity);
            }
            if (collision.gameObject.layer == 10)
            {
                Instantiate(bg3, new Vector3(20.23f, 0.02f, 0), Quaternion.identity);
            }
            Destroy(collision.gameObject);
        }
    }
}
