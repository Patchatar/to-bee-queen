﻿using System.Collections;
using System.Collections.Generic;
using System.Numerics;
using UnityEngine;

public class BackgroundMove : MonoBehaviour
{

    public float moveSpeed;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {

    }

    private void FixedUpdate()
    {
        //GetComponent<Rigidbody2D>().velocity = new Vector2(moveSpeed, GetComponent<Rigidbody2D>().velocity.x);
        transform.Translate(-moveSpeed * Time.deltaTime, 0, 0);
    }
}
